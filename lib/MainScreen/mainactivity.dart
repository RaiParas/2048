import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:number_game/MainScreen/logics.dart';
import 'package:swipedetector/swipedetector.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // int width;

  List<List<int>> values = [
    [0, 2, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 2, 0, 0],
  ];

  double width, height;
  double contentWidth;
  DateTime backButtonPressTime;

  static const snackBarDuration = Duration(seconds: 3);

  final snackBar = SnackBar(
    content: Text('Press back again to Exit'),
    duration: snackBarDuration,
  );

  final scaffoldKey = GlobalKey<ScaffoldState>();

  void addMoreTilesandcheckforGamePlay() {
    int numbers = checkifZeroExist(values);
    this.setState(() {
      values = createNextMove(values, numbers);
    });
    numbers = checkifZeroExist(values);
    if (numbers == 0) {
      bool isPlayable = checkIsPlayable(values);
      if (!isPlayable) {
        showAlertDialog(context);
      }
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget newButton = FlatButton(
      child: Text(
        "New Game",
        style: TextStyle(color: Colors.green),
      ),
      onPressed: () {
        List<List<int>> newGame = [
          [0, 2, 0, 0],
          [0, 0, 0, 0],
          [0, 0, 0, 0],
          [0, 2, 0, 0],
        ];
        this.setState(() {
          values = newGame;
        });
        Navigator.pop(context);
      },
    );
    Widget exitButton = FlatButton(
      child: Text(
        "Exit",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Game Over",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Want to Start new Game"),
      actions: [newButton, exitButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    contentWidth = width / 4.5;
    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          DateTime now = DateTime.now();
          if (backButtonPressTime == null || now.difference(backButtonPressTime) > Duration(seconds: 2)) {
            backButtonPressTime = now;
            Fluttertoast.showToast(
                msg: "Press Again to exit",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black26,
                textColor: Colors.white,
                fontSize: 16.0);
            return Future.value(false);
          }
          return Future.value(true);
        },
        child: Center(
          child: SwipeDetector(
            child: Container(
              width: 4 * contentWidth,
              height: 4 * contentWidth,
              decoration: BoxDecoration(
                border: Border.all(),
              ),
              child: CustomPaint(
                painter: OpenPainter(values, contentWidth),
              ),
            ),
            onSwipeUp: () {
              this.setState(() {
                values = swipedUp(values);
              });
              addMoreTilesandcheckforGamePlay();
            },
            onSwipeDown: () {
              this.setState(() {
                values = swipedDown(values);
              });
              addMoreTilesandcheckforGamePlay();
            },
            onSwipeLeft: () {
              this.setState(() {
                values = swipedLeft(values);
              });
              addMoreTilesandcheckforGamePlay();
            },
            onSwipeRight: () {
              this.setState(() {
                values = swipedRight(values);
              });
              addMoreTilesandcheckforGamePlay();
            },
            swipeConfiguration: SwipeConfiguration(
              verticalSwipeMinVelocity: 100.0,
              verticalSwipeMinDisplacement: 50.0,
              verticalSwipeMaxWidthThreshold: 100.0,
              horizontalSwipeMaxHeightThreshold: 50.0,
              horizontalSwipeMinDisplacement: 50.0,
              horizontalSwipeMinVelocity: 200.0,
            ),
          ),
        ),
      ),
    );
  }
}

class OpenPainter extends CustomPainter {
  List<List<int>> data = new List();
  double contentWidth;
  OpenPainter(List<List<int>> values, double contentW) {
    data = values;
    contentWidth = contentW;
  }

  @override
  void paint(Canvas canvas, Size size) {
    var paint1 = Paint()
      ..color = Color(0xff638965)
      ..style = PaintingStyle.stroke;
    for (double i = 0; i < 4 * contentWidth; i += contentWidth) {
      for (double j = 0; j < 4 * contentWidth; j += contentWidth) {
        canvas.drawRect(Offset((i + 0.0), (j + 0.0)) & Size(contentWidth, contentWidth), paint1);
      }
    }
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        if (data[i][j] != 0) {
          drawBoxes(canvas, j, i, data[i][j]);
        }
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;

  void drawBoxes(Canvas canvas, int x, int y, int value) {
    var paintbox = Paint()
      ..color = getColorCode(value)
      ..style = PaintingStyle.fill;
    drawTxt(canvas, value, (x + 0.0) * contentWidth, (y + 0.0) * contentWidth, paintbox);
  }

  Color getColorCode(int value) {
    if (value == 2)
      return Color(0xFF2196F3);
    else if (value == 4)
      return Color(0xFF81205D);
    else if (value == 8)
      return Color(0xFF22466C);
    else if (value == 16)
      return Color(0xFFA56529);
    else if (value == 32)
      return Color(0xFF581124);
    else if (value == 64)
      return Color(0xFF4EA513);
    else if (value == 128)
      return Color(0xFFD37272);
    else if (value == 256)
      return Color(0xFF041620);
    else if (value == 512)
      return Color(0xFFE8272A);
    else if (value == 1024)
      return Color(0xFF6E1CDF);
    else if (value == 2048) return Color(0xFF000000);
    return Color(0xFFFFA726);
  }

  void drawTxt(Canvas canvas, int txt, double x, double y, var paintbox) {
    canvas.drawRect(Offset(x, y) & Size(contentWidth, contentWidth), paintbox);
    TextSpan span = new TextSpan(
      style: new TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
      text: txt.toString(),
    );
    TextPainter tp = new TextPainter(
      text: span,
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );
    tp.layout(minWidth: contentWidth, maxWidth: contentWidth);
    tp.paint(canvas, Offset(x, y + (contentWidth / 2 - 10)));
  }
}
