import 'dart:math';

List<List<int>> swipedUp(List<List<int>> data) {
  List<List<int>> array = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ];
  int posx, posy;
  for (int i = 0; i < 4; i++) {
    posx = 0;
    posy = 1;
    while (posy < 4) {
      if (data[posx][i] == 0) {
        while (data[posy][i] == 0 && posy < 4) {
          posy++;
          if (posy == 4) break;
        }
        if (posy == 4) break;
        data[posx][i] = data[posy][i];
        data[posy][i] = 0;
        posy++;
      } else if (data[posx][i] == data[posy][i]) {
        data[posx][i] += data[posy][i];
        data[posy][i] = 0;
        posx++;
        posy++;
      } else if (data[posy][i] == 0)
        posy++;
      else {
        posx++;
        data[posx][i] = data[posy][i];
        posy++;
      }
    }
    posx++;
    while (posx < 4) {
      data[posx][i] = 0;
      posx++;
    }
  }
  array = data;

  return array;
}

List<List<int>> swipedDown(List<List<int>> data) {
  List<List<int>> array = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ];
  int posx, posy;
  for (int i = 0; i < 4; i++) {
    posx = 3;
    posy = 2;
    while (posy >= 0) {
      if (data[posx][i] == 0) {
        while (data[posy][i] == 0 && posy >= 0) {
          posy--;
          if (posy == -1) break;
        }
        if (posy == -1) break;
        data[posx][i] = data[posy][i];
        data[posy][i] = 0;
        posy--;
      } else if (data[posx][i] == data[posy][i]) {
        data[posx][i] += data[posy][i];
        data[posy][i] = 0;
        posx--;
        posy--;
      } else if (data[posy][i] == 0)
        posy--;
      else {
        posx--;
        data[posx][i] = data[posy][i];
        posy--;
      }
    }
    posx--;
    while (posx >= 0) {
      data[posx][i] = 0;
      posx--;
    }
  }
  array = data;

  return array;
}

List<List<int>> swipedRight(List<List<int>> data) {
  List<List<int>> array = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ];
  int posx, posy;
  for (int i = 0; i < 4; i++) {
    posx = 3;
    posy = 2;
    while (posy >= 0) {
      if (data[i][posx] == 0) {
        while (data[i][posy] == 0 && posy >= 0) {
          posy--;
          if (posy == -1) break;
        }
        if (posy == -1) break;
        data[i][posx] = data[i][posy];
        data[i][posy] = 0;
        posy--;
      } else if (data[i][posx] == data[i][posy]) {
        data[i][posx] += data[i][posy];
        data[i][posy] = 0;
        posx--;
        posy--;
      } else if (data[i][posy] == 0)
        posy--;
      else {
        posx--;
        data[i][posx] = data[i][posy];
        posy--;
      }
    }
    posx--;
    while (posx >= 0) {
      data[i][posx] = 0;
      posx--;
    }
  }
  array = data;

  return array;
}

List<List<int>> swipedLeft(List<List<int>> data) {
  List<List<int>> array = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ];
  int posx, posy;
  for (int i = 0; i < 4; i++) {
    posx = 0;
    posy = 1;
    while (posy < 4) {
      if (data[i][posx] == 0) {
        while (data[i][posy] == 0 && posy < 4) {
          posy++;
          if (posy == 4) break;
        }
        if (posy == 4) break;
        data[i][posx] = data[i][posy];
        data[i][posy] = 0;
        posy++;
      } else if (data[i][posx] == data[i][posy]) {
        data[i][posx] += data[i][posy];
        data[i][posy] = 0;
        posx++;
        posy++;
      } else if (data[i][posy] == 0)
        posy++;
      else {
        posx++;
        data[i][posx] = data[i][posy];
        posy++;
      }
    }
    posx++;
    while (posx < 4) {
      data[i][posx] = 0;
      posx++;
    }
  }
  array = data;

  return array;
}

int checkifZeroExist(List<List<int>> data) {
  int xeros = 0;
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++) if (data[i][j] == 0) xeros++;

  return xeros;
}

List<List<int>> createNextMove(List<List<int>> data, int numbers) {
  Random rnd;
  int min = 0;
  rnd = new Random();
  int random = min + rnd.nextInt(numbers - 0);
  int flag = 0;
  print(random);
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      if (flag == random && data[i][j] == 0) {
        data[i][j] = 2;
        flag++;
      } else if (data[i][j] == 0) flag++;
    }
  }
  return data;
}

bool checkIsPlayable(List<List<int>> data) {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 3; j++) {
      if (data[i][j] == data[i][j + 1]) return true;
      if (data[j][i] == data[j + 1][i]) return true;
    }
  }
  return false;
}
