import 'package:flame/util.dart';
import 'package:flutter/material.dart';
import 'package:number_game/MainScreen/mainactivity.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Future<void> flameWork() async {
    Util flameUtil = Util();
    await flameUtil.setOrientation(DeviceOrientation.portraitUp);
  }

  @override
  Widget build(BuildContext context) {
    flameWork();
    return MaterialApp(
      title: 'Game Development',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: '2048'),
    );
  }
}
